import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

// import styles from "./assets/jss/components.js";

// const useStyles = makeStyles(styles);

const useStyles = makeStyles(theme => ({
   main: {
    border: `2px solid ${theme.palette.divider}`, 
  //  backgroundColor: theme.palette.grey[200],
     backgroundColor: theme.palette.background.paper,
    //backgroundColor: theme.palette.white,
    height: '100vh'
  },
}));


export default function Blog() {
 
  const classes = useStyles();

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="lg">
        
        <Typography component="div" className={classes.main} />

      </Container>
    </React.Fragment>
  );
}