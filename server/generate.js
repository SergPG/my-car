var faker = require('faker');

var database = { customers: [],
                 projects: [] };

for (var i = 1; i<= 2; i++) {
  database.customers.push({
    id: i,
    num: faker.random.number({min:103, max:180}),
    name: faker.name.firstName() + ' ' + faker.name.lastName(),
    email: faker.internet.email(),
    phone: faker.phone.phoneNumber(),
    city: faker.address.city(),
    country: faker.address.country(),
    title: faker.name.title()
  });
}

for (var i = 1; i<= 18; i++) {
  database.projects.push({
    id: i,
    title: faker.lorem.sentence(2),
    description: faker.lorem.paragraph(),
    image: 'https://picsum.photos/id/'+ faker.random.number({min:1041, max:1085}) +'/900/650',
    

  });
}

console.log(JSON.stringify(database));